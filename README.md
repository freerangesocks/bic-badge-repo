Welcome to the BIC Badge Code Repository


**In order to use this Hardware Badge and even improve on the project please use the included files to flash the badge and or create new functionality.**

**/// Environment Setup ///**
- this is needed to interact with the badge, and interface via the arduino ide.

The code provided by waveshare(to Drive the Screen...does not work and is useless )
- https://www.waveshare.com/wiki/E-Paper_ESP8266_Driver_Board

A few good videos that explains what is happening:
- https://randomnerdtutorials.com/esp32-esp-now-wi-fi-web-server
- https://www.instructables.com/Programming-a-HTTP-Server-on-ESP-8266-12E/


1. Install Arduino IDE
- https://www.arduino.cc/en/software

2. Follow this tutorial to install the esp8266 libraries
- https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/

3. Install Arduino Additional Libraries Needed
- Arduino_JSON [BETA]
- ESP8266
- ESP8266WiFi
- espnow
- arduino-esp32
- https://github.com/espressif/arduino-esp32/tree/master/libraries
- https://github.com/me-no-dev/ESPAsyncWebServer
- https://github.com/me-no-dev/AsyncTCP


4. Install ESP32 Drivers on to your system
- https://www.silabs.com/developers/usb-to-uart-bridge-vcp-drivers

5. Plugin the Badge and set upload speed 115200 ensure the com port is selected in the arduino sketch

6. ensure the **names.h** , and  **index_normal.h** file is in the same folder of **_NormalBadge_ArduinoFile.ino_**

